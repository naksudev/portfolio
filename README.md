![Astro](https://img.shields.io/badge/astro-%232C2052.svg?style=for-the-badge&logo=astro&logoColor=white) [![Netlify Status](https://api.netlify.com/api/v1/badges/65b45aa4-692c-4645-b673-4449aa9ef5b0/deploy-status)](https://app.netlify.com/sites/naksu-portfolio/deploys)

# Portfolio
Welcome to the source code of my portfolio!

## Requirements for building
Not there yet.

## Contribution
You may contribute for the following things :
- Responsive Design
- UX/UI
- Typo correction

Every MRs/PRs that completely modifies the content of the portfolio will be closed. 
