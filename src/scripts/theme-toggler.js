function setPreferredTheme() {
    const localStorageTheme = localStorage.getItem("theme");
    const systemSettingsDark = window.matchMedia("(prefers-color-scheme: dark)");

    return localStorageTheme || (systemSettingsDark.matches ? "dark" : "light");
}

let currentThemeSetting = setPreferredTheme();

document.documentElement.setAttribute("data-theme", currentThemeSetting);

const button = document.querySelector("[data-theme-toggle]");

button.addEventListener("click", () => {
    document.documentElement.classList.add("theme-transition");
    const newTheme = currentThemeSetting === "dark" ? "light" : "dark";

    document.documentElement.setAttribute("data-theme", newTheme);
    localStorage.setItem("theme", newTheme);
    currentThemeSetting = newTheme;

    setTimeout(() => {
        document.documentElement.classList.remove("theme-transition");
    }, 500); 
});

window.addEventListener("DOMContentLoaded", () => {
    document.documentElement.classList.remove("theme-transition");
});
