# Specifications of the portfolio

## Pages
The goal of my portfolio is to be light and not overwhelming as the user goes through it.

- **index** - mostly about me and my career
- **works** - show off my personal and professionnal projects
- **contact** - my contact and social medias informations

## Features
Some features for my portfolio that will improve the UX/UI.

- **theme switcher** - A dark and light toggler
- **responsive design** - a must-have for a website
- **language switcher** - 🇫‍🇷 // 🇺‍🇸

## Deployment
I'm going to host my portfolio on **Netlify** linked to a GitLab repo with my own domain.

## References
- https://www.craftz.dog/
